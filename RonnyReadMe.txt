You can start the game with one of the main buttons (X for left controller, A for right controller) but only if you are not holding a gun. No cheating.

Pistols can't be reloaded. When it's empty throw it away and grab a new one. If you're accurate you can also throw it at a target for bonus points.

Have fun :)